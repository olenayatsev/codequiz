﻿namespace CodeQuiz.Models
{
    public class AppSettings
    {
        public string BaseUrlApi { get; set; }
        public string InitialTextFilePath { get; set; }
    }
}
