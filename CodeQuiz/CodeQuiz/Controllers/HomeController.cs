﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using CodeQuiz.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

using FullTask = CodeQuiz.Models.FullTask;

namespace CodeQuiz.Controllers
{
    public class HomeController : Controller
    {
        private readonly AppSettings _settings;
        private static CurrentTask _currentTask;
        private static int _tasksCounter;

        public HomeController(IOptions<AppSettings> settings)
        {
            _settings = settings.Value;
            if(_currentTask == null)
                _currentTask = new CurrentTask();
        }

        public IActionResult Index()
        {
            var path = _settings.BaseUrlApi + "/task/";
            try
            {
                var tasks = JsonConvert.DeserializeObject<List<FullTask>>(GetObject(path).Result);
                ViewBag.MyTasks = tasks;
                _tasksCounter = tasks.Count;
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View("Error");
            }
            
            return View();
        }

        public IActionResult Task(int id)
        {
            ViewBag.AlertClass = "";
            ViewBag.AlertText = "";

            if (id == _currentTask.Id)
                return View(_currentTask);
            
            var path = _settings.BaseUrlApi + "/task/" + id;
            FullTask task;
            try
            {
                task = JsonConvert.DeserializeObject<FullTask>(GetObject(path).Result);
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View("Error");
            }

            _currentTask = new CurrentTask()
            {
                    Id = task.Id,
                    ShortDescription = task.ShortDescription,
                    FullDescription = task.FullDescription,
                    Name = task.Name,
                    Completed = false,
                    Code = System.IO.File.ReadAllText(_settings.InitialTextFilePath),
            };
            
            return View(_currentTask);
        }
        
        public IActionResult Check(CurrentTask values)
        {
            _currentTask.Code = values.Code;

            string path = _settings.BaseUrlApi + "/task";
            var request = new CheckTaskRequest()
            {
                Code = _currentTask.Code,
                Id = _currentTask.Id
            };

            CheckTaskResponse response;

            try
            {
                response = JsonConvert.DeserializeObject<CheckTaskResponse>(PostObject(path, request).Result);
            }
            catch (Exception e)
            {
                ViewBag.Message = e.Message;
                return View("Error");
            }
            
            if (response.Result)
            {
                ViewBag.AlertClass = "alert alert-success";
                ViewBag.AlertText = "Success";
                ViewBag.TasksCounter = _tasksCounter;
                _currentTask.Completed = true;
            }
            else
            {
                ViewBag.AlertClass = "alert alert-danger";
                ViewBag.AlertText = "Error";         
                _currentTask.Completed = false;
            }

            ViewBag.Message = response.Message;

            return View("Task", _currentTask);
        }

        public IActionResult Congratulation()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public static async Task<string> GetObject(string path)
        {
            var response = await HttpHelper.Get(path);
            var result = HttpHelper.ContentAsString(response);

            return result.Result;
        }

        public static async Task<string> PostObject(string path, object value)
        {
            var response = await HttpHelper.Post(path, value);
            var result = HttpHelper.ContentAsString(response);

            return result.Result;
        }

    }
}
