﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using Microsoft.Extensions.Options;
using TestRunner.Abstract;
using TestRunner.Logic;
using TestRunner.Models;

namespace APIQuiz.Controllers
{
    [Route("[controller]")]
    public class TaskController : Controller
    {
        private readonly IBusinessLogic _tasksManager;
        private readonly ConfigurationPaths _path;

        public TaskController(IBusinessLogic service, IOptions<ConfigurationPaths> paths)
        {   
            _tasksManager = service;
            _path = paths.Value;
        }

        //GET api/task
        [HttpGet]
        public ActionResult<QuizTask> Get()
        {
            List<QuizTask> allTasks = new List<QuizTask>();
            int id = 1;
            
            while(_tasksManager.GetTask(id, _path) != null)
                allTasks.Add(_tasksManager.GetTask(id++, _path));

            var res = new ObjectResult(JsonConvert.SerializeObject(allTasks));
            res.ContentTypes.Add(new MediaTypeHeaderValue("application/json"));
            return res;
        }

        // GET api/task/5
        [HttpGet("{id}")]
        public ActionResult<QuizTask> Get(int id)
        {
            var res = new ObjectResult(JsonConvert.SerializeObject(_tasksManager.GetTask(id, _path)));
            res.ContentTypes.Add(new MediaTypeHeaderValue("application/json"));
            return res;
        }

        // POST api/task
        [HttpPost]
        public ActionResult<CheckTaskResponse> Post([FromBody] CheckTaskRequest value)
        {
            var res = new ObjectResult(JsonConvert.SerializeObject(_tasksManager.CheckCode(value, _path)));
            res.ContentTypes.Add(new MediaTypeHeaderValue("application/json"));
            return res;
        }
        
        // PUT api/task/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/task/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
