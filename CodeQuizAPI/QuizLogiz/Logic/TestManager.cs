﻿using System;
using System.IO;
using TestRunner.Models;
using System.Text.RegularExpressions;
using TestRunner.Abstract;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using System.Linq;
using Microsoft.CodeAnalysis.Emit;
using System.Collections.Generic;
using System.Reflection;

namespace TestRunner.Logic
{
    
    public class TaskManager : IBusinessLogic
    {

        public string[] TestValues;
        public string[] TestResults;
        
        // Running test on users code
        public ProcessResultModel RunTest(string testValues, string expectation, MemoryStream ms)
        {
            ProcessResultModel result = new ProcessResultModel();

            ms.Seek(0, SeekOrigin.Begin);
            Assembly assembly = Assembly.Load(ms.ToArray());

            Type type = assembly.GetType("Program.Program");

            object obj = Activator.CreateInstance(type);

            result.Result = type.InvokeMember("Function",
                BindingFlags.Default | BindingFlags.InvokeMethod,
                null,
                obj,
                new object[] { testValues }).ToString();

            if (result.Result.CompareTo(expectation) == 0)
            {
                result.ExitCode = 0;
                result.Result = " Passed\nTest Values: " + testValues +"\nExpected Output: " + expectation + "\nOutput: " + result.Result + "\n";
            }
            else
            {
                result.ExitCode = 1;
                result.Result = " Didn't Pass\nTest Values: " + testValues + "\nExpected Output: " + expectation  + "\nOutput: " +  result.Result + "\n";             
            }

            return result;
        }


        //Reading test and expeced result values from file

        private void SetTests(int id, ConfigurationPaths paths)
        {
            var path = paths.FolderPath;

            path += "\\" + id.ToString() + "\\";

            if(!File.Exists(path + "Tests.txt") || !File.Exists(path + "Results.txt"))
            {
                throw new Exception("Test file doesn`t exist");
            }
            else
            {
                TestValues = System.IO.File.ReadAllLines(path + "Tests.txt");
                TestResults = System.IO.File.ReadAllLines(path + "Results.txt");
            }
        }


        public QuizTask GetTask(int id, ConfigurationPaths paths)
        {
            var path = paths.FolderPath;
            path += "\\" + id + "\\";

            QuizTask task = new QuizTask() { Id = id };

            if (!Directory.Exists(path) || !File.Exists(path + "Name.txt") || !File.Exists(path + "ShortDescription.txt") || !File.Exists(path + "Description.txt"))
            {
                return null;
            }
            else
            {
                task.Name = System.IO.File.ReadAllText(path + "Name.txt");
                task.ShortDescription = System.IO.File.ReadAllText(path + "ShortDescription.txt");
                task.FullDescription = System.IO.File.ReadAllText(path + "Description.txt");
                return task;
            }
        }

        public CheckTaskResponse CheckCode(CheckTaskRequest request, ConfigurationPaths paths)
        {
            CheckTaskResponse answer = new CheckTaskResponse() { Id = request.Id, Result = true , Message = ""};

            if (request.Code == null)
            {
                answer.Message = "Please, enter your code";
                answer.Result = false;
                return answer;
            }
            SetTests(request.Id, paths);
            SyntaxTree syntaxTree = CSharpSyntaxTree.ParseText(request.Code);

            string assemblyName = Path.GetRandomFileName();
            MetadataReference[] references = new MetadataReference[]
            {
                MetadataReference.CreateFromFile(typeof(object).Assembly.Location),
                MetadataReference.CreateFromFile(typeof(Enumerable).Assembly.Location)
            };

            CSharpCompilation compilation = CSharpCompilation.Create(
                assemblyName,
                syntaxTrees: new[] { syntaxTree },
                references: references,
                options: new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary));

            using (var ms = new MemoryStream())
            {
                EmitResult result = compilation.Emit(ms);

                if (!result.Success)
                {
                    IEnumerable<Diagnostic> failures = result.Diagnostics.Where(diagnostic =>
                        diagnostic.IsWarningAsError ||
                        diagnostic.Severity == DiagnosticSeverity.Error);
                    answer.Result = false;
                    foreach (Diagnostic diagnostic in failures)
                    {
                        answer.Message += diagnostic.Id + diagnostic.GetMessage() + "\n";
                    }
                }
                else
                {
                    for (int i = 0; i < TestValues.Length && i < TestResults.Length; i++)
                    {
                        var res = RunTest(TestValues[i], TestResults[i], ms);
                        answer.Message += "\nTest № " + (i + 1).ToString();
                        if (res.ExitCode != 0)
                        {
                            answer.Result = false;
                        }
                        answer.Message += res.Result;
                    }
                }
                return answer;
            }
        }
    }
}
