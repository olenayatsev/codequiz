﻿namespace TestRunner.Models
{
    public class ConfigurationPaths
    {
        public string CompilerPath { get; set; }
        public string FolderPath { get; set; }
        public string CsFilePath { get; set; }
    }
}
