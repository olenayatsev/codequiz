﻿namespace TestRunner.Abstract
{
    public interface IBusinessLogic
    {
        TestRunner.Logic.QuizTask GetTask(int id, Models.ConfigurationPaths paths);
        TestRunner.Models.CheckTaskResponse CheckCode(Models.CheckTaskRequest request, Models.ConfigurationPaths paths);
    }
}
